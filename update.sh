#/usr/bin/bash


hosted_zone_id=$1;
domain_name=$2;

aws route53 list-resource-record-sets --hosted-zone-id $hosted_zone_id --query "ResourceRecordSets[?Type == 'A']" &> ./previous-record-set.json;

public_ipv4_address=`curl http://169.254.169.254/latest/meta-data/public-ipv4 2>/dev/null`

read -r -d '' my_string << EOF
{
   "HostedZoneId": "{ $hosted_zone_id }",
   "ChangeBatch": {
      "Comment": "Updated IP Address",
      "Changes": [
         {
            "Action": "UPSERT",
            "ResourceRecordSet": {
               "Name": "{ $domain_name }",
               "Type": "A",
               "TTL": 60,
               "ResourceRecords": [
                  { "Value": "{ $public_ipv4_address }" }
               ]
           }
         }
      ]
  }
}

aws route53 change-resource-record-sets --cli-input-json $my_string &> change-record-set.out

exit 0;
